defmodule RepoFinder do
  import RepoFinder.TextFormatting
  alias RepoFinder.Logging

  @moduledoc """
  Documentation for RepoFinder.
  """

  @doc """
  main function — interprets provided parameters and executes a directory search if they are valid. Prints out the results of the search.

  ## Examples

      iex> RepoFinder.main(["repo_finder", "~/Projects", "my_project"])
      ~/Projects/personal/my_project
      :ok

  """
  @spec main([binary]) :: :ok
  def main(args) do
    case parse_args(args) do
      {directory_path, search_term, logging, ignorables} ->
        run_search(directory_path, search_term, logging, ignorables)

      :error ->
        red_bold_puts(
          "\nHmmm...something doesn't look right. Make sure you include both a directory to search in and a search term when running repo_finder.\n"
        )

        red_bold_puts(
          "For example, to search for a directory called 'my_project' inside the '~/Projects' directory, you could use this command:\n"
        )

        white_bold_puts("repo_finder ~/Projects my_project\n")
    end
  end

  @spec run_search(binary, binary, boolean, [binary]) :: :ok
  defp run_search(directory_path, search_term, logging, ignorables) do
    if logging, do: Logging.introduce_script(directory_path, search_term, logging, ignorables)

    is_match? = match_func_for(search_term)

    if logging do
      directory_path
      |> directories_of_interest_in(logging, ignorables)
      |> Enum.filter(is_match?)
      |> Enum.sort_by(match_quality_func_for(search_term))
      |> Logging.puts_matching_directories_info()
      |> List.first()
      |> Logging.puts_first_match_info()
    else
      directory_path
      |> directories_of_interest_in(logging, ignorables)
      |> Enum.filter(is_match?)
      |> Enum.sort_by(match_quality_func_for(search_term))
      |> List.first()
      |> IO.puts()
    end
  end

  @spec match_func_for(binary) :: (binary -> boolean)
  defp match_func_for(search_term) do
    downcased_search_term = String.downcase(search_term)

    fn directory_path ->
      directory_name = standardized_directory_name(directory_path)
      abbreviation = first_letter_of_each_word(directory_name)

      String.starts_with?(abbreviation, downcased_search_term) ||
        String.contains?(directory_name, downcased_search_term)
    end
  end

  @spec standardized_directory_name(binary) :: binary
  defp standardized_directory_name(directory_path) do
    directory_path
    |> String.replace(~r/.*\//, "")
    |> String.downcase()
  end

  @spec match_quality_func_for(binary) :: (binary -> non_neg_integer)
  defp match_quality_func_for(search_term) do
    downcased_search_term = String.downcase(search_term)

    fn directory ->
      directory_name = standardized_directory_name(directory)
      abbreviation = first_letter_of_each_word(directory_name)

      if String.starts_with?(abbreviation, downcased_search_term) do
        # Best case is an abbreviation match
        0
      else
        # Next best case does better the closer it is to the beginning of the string
        distance_from_start(directory_name, downcased_search_term)
      end
    end
  end

  @spec distance_from_start(binary, binary) :: nil | non_neg_integer
  defp distance_from_start(string, substring) do
    case String.split(string, substring, parts: 2) do
      [left, _] ->
        String.length(left)

      [_] ->
        nil
    end
  end

  @spec first_letter_of_each_word(binary) :: binary
  defp first_letter_of_each_word(string) do
    string
    |> String.split(~r{-|_| })
    |> Enum.map(&String.first/1)
    |> Enum.join("")
  end

  @spec directories_of_interest_in(binary, boolean, [binary]) :: [binary]
  defp directories_of_interest_in(directory_path, with_logging, ignorables) do
    if with_logging, do: IO.puts(String.replace_leading(directory_path, Path.expand("~"), "~"))

    if Enum.any?(ignorables, &String.ends_with?(directory_path, &1)) do
      []
    else
      cond do
        is_repo?(directory_path) ->
          [directory_path]

        is_an_ignorable_directory?(directory_path) ->
          # Skip some kinds of directories entirely
          []

        true ->
          directory_path
          |> subdirectories_in
          |> Enum.flat_map(&directories_of_interest_in(&1, with_logging, ignorables))
          |> List.insert_at(0, directory_path)
      end
    end
  end

  @spec is_an_ignorable_directory?(binary) :: boolean
  defp is_an_ignorable_directory?(directory_path) do
    String.contains?(
      directory_path,
      ["/node_modules", "/venv", "/_build", "/deps", "/elm-stuff", "/.elixir_ls", "/__pycache__"]
    )
  end

  @spec is_repo?(binary) :: boolean
  defp is_repo?(directory_path) do
    directory_path
    |> subdirectories_in
    |> Enum.any?(&String.ends_with?(&1, ".git"))
  end

  @spec subdirectories_in(binary) :: [binary]
  defp subdirectories_in(directory_path) do
    directory_path
    |> Path.expand()
    |> File.ls!()
    |> Enum.map(prepend_path(directory_path))
    |> Enum.filter(&File.dir?/1)
  end

  @spec prepend_path(binary) :: (binary -> binary)
  defp prepend_path(containing_directory) do
    fn item -> Path.join(containing_directory, item) end
  end

  @spec parse_args([binary]) :: {binary, binary, boolean, [binary]} | :error
  defp parse_args(args) do
    case OptionParser.parse(args,
           aliases: [l: :log, i: :ignore],
           strict: [log: :boolean, ignore: :string]
         ) do
      {options, [directory, search_term], _} ->
        {
          directory,
          search_term,
          options[:log],
          parse_ignorables(options[:ignore])
        }

      _ ->
        :error
    end
  end

  @spec parse_ignorables(binary) :: [binary]
  defp parse_ignorables(string) do
    case string do
      nil -> []
      string -> String.split(string, ~r{\s*\,\s*})
    end
  end
end
