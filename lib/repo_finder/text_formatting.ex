defmodule RepoFinder.TextFormatting do
  def red_bold_puts(string) do
    bold_puts "#{IO.ANSI.red}#{string}#{IO.ANSI.default_color}"
  end
  
  def white_bold_puts(string) do
    bold_puts "#{IO.ANSI.white}#{string}#{IO.ANSI.default_color}"
  end

  def bold_puts(string) do
    IO.puts "#{IO.ANSI.bright}#{string}#{IO.ANSI.normal}"
  end
end