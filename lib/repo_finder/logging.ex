defmodule RepoFinder.Logging do
  import RepoFinder.TextFormatting

  def introduce_script(directory_path, search_term, logging, ignore) do
    bold_puts "\nRunning repo_finder...\n"
    bold_puts "Settings:"
    IO.puts "search: #{inspect(search_term)}"
    IO.puts "directory: #{inspect(directory_path)}"
    IO.puts "logging: #{inspect(logging)}"
    IO.puts "ignore: #{inspect(ignore)}\n"
    bold_puts "Scanning..."
  end

  def puts_matching_directories_info(directories) do
    bold_puts "\nAll directories matching search query:"
    IO.puts formatted_list_of_directories(directories)
    IO.puts ""
    directories
  end

  def puts_first_match_info(first_match) do
    if first_match do
      bold_puts "First match:"
      IO.puts shorthand_directory_name(first_match)
      IO.puts ""
    else
      bold_puts "No matches."
    end

    first_match
  end

  defp formatted_list_of_directories(directories) do
    directories
    |> Enum.map(&shorthand_directory_name/1)
    |> Enum.join("\n")
  end

  defp shorthand_directory_name(directory) do
    String.replace_leading(directory, Path.expand("~"), "~")
  end
end