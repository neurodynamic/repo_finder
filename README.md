# RepoFinder

A console tool for searching for repos or repo-containing directories. I use it for quick folder-switching in the console.

Search example (will match a `my_project` repo inside the `~/Projects` directory):
```
repo_finder ~/Projects my_project
```

Abbreviation search example (will *also* match a `my_project` repo inside the `~/Projects` directory):
```
repo_finder ~/Projects mp
```

Search example with logging:
```
repo_finder ~/Projects my_project --log
```

Search example for ignoring some directories (won't search in `directory_to_ignore`, or `another_directory_to_ignore`):
```
repo_finder ~/Projects my_project --ignore="directory_to_ignore,another_directory_to_ignore"
```



## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `repo_finder` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:repo_finder, "~> 0.1.0"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/repo_finder](https://hexdocs.pm/repo_finder).

## Development

### To Compile

```
mix escript.build
```

### To Run

```
./repo_finder ~/Projects my_project
```